package gui;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JDesktopPane;
import javax.swing.JSeparator;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.JRadioButtonMenuItem;

public class CanOpenRechner extends JFrame {

	
    String strVersionsNummer = "1.3";
    boolean cobid = false;
    boolean nodeid = true;
    JRadioButtonMenuItem rdbtnmntmCobid = new JRadioButtonMenuItem("Cob-ID");
    JRadioButtonMenuItem rdbtnmntmNodeid = new JRadioButtonMenuItem("Node-ID");
    JLabel lblEmergency = new JLabel("Emergency");
    JLabel lblEmergencyErgebnis = new JLabel("");
    JLabel lblHeartbeatErgebnis = new JLabel("");
    JLabel lblPDO1Ergebnis = new JLabel("");
    JLabel lblPDO2Ergebnis = new JLabel("");
    JLabel lblPDO3Ergebnis = new JLabel("");
    JLabel lblPDO4Ergebnis = new JLabel("");
    JLabel lblSDO1Ergebnis = new JLabel("");
    JLabel lblSDO2Ergebnis = new JLabel("");
    JLabel lblSDO3Ergebnis = new JLabel("");
    JLabel lblSDO4Ergebnis = new JLabel("");
    JLabel lblSDOrx = new JLabel("");
    JLabel lblSDOtx = new JLabel("");
    JLabel lblNodeIDErgebnis = new JLabel("");
    JLabel lblNodeTyp = new JLabel("");
	JLabel lblNodeidErmitteln = new JLabel("Node-ID ermitteln");
	JLabel lblNewLabel_1 = new JLabel("Node-ID");
	JLabel lblTyp = new JLabel("Typ");
	JTextField tfHextoNode = new JTextField();
	JLabel lblSdorx = new JLabel("SDO (rx)");
	JLabel lblSdotx = new JLabel("SDO (tx)");
	JLabel lblPdo = new JLabel("PDO1 (tx)");
	JLabel lblPdo_1 = new JLabel("PDO2 (tx)");
	JLabel lblPdo_2 = new JLabel("PDO3 (tx)");
	JLabel lblPdo_3 = new JLabel("PDO4 (tx)");
	JLabel lblSdo = new JLabel("PDO1 (rx)");
	JLabel lblSdo_1 = new JLabel("PDO2 (rx)");
	JLabel lblSdo_2 = new JLabel("PDO3 (rx)");
	JLabel lblHeartbeat = new JLabel("Heartbeat");
	JLabel lblNodeID = new JLabel("Cob-ID ermitteln");
	JTextField tfNodeID = new JTextField();
	JButton btnUmrechnen = new JButton("Umrechnen");
	JButton btnUmrechnen2 = new JButton("Umrechnen");
	JLabel lblSdo_3 = new JLabel("PDO4 (rx)");


    private JPanel contentPane;
    
    Image icon = null;

    /**
     * Launch the application.
     */
    public static void main(String[] args) throws URISyntaxException {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    CanOpenRechner frame = new CanOpenRechner();
		    frame.setVisible(true);
		    
		    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		  
		    SwingUtilities.updateComponentTreeUI(frame);

		    /*updateComponentTreeUI(frame); */

		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the frame.
     * 
     * @throws URISyntaxException
     * @throws IOException
     */

    private static void open() throws URISyntaxException {
	if (Desktop.isDesktopSupported()) {
	    try {
		final URI uri = new URI(
			"http://www.raspitux.de/canopen-rechner/");
		Desktop.getDesktop().browse(uri);
	    } catch (IOException e) { /* TODO: error handling */
	    }
	} else { /* TODO: error handling */
	}
    }

    public CanOpenRechner() throws IOException {
	setResizable(false);
	setTitle("CanOpen Rechner V" + strVersionsNummer);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 450, 289);
	
	umschalten(1);
	
	JMenuBar menuBar = new JMenuBar();
	setJMenuBar(menuBar);
	
	JMenu mnMen = new JMenu("Men\u00FC");
	menuBar.add(mnMen);
	
	JMenuItem mntmBeenden = new JMenuItem("Beenden");
	mntmBeenden.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	});
	
	
	//JRadioButtonMenuItem rdbtnmntmCobid = new JRadioButtonMenuItem("Cob-ID");
	
	rdbtnmntmCobid.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			/** Cob-ID pressed **/

			umschalten(1);
		}
	});
	mnMen.add(rdbtnmntmCobid);
	
	rdbtnmntmNodeid.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			/** Node-ID pressed **/
			
			umschalten(2);
			
		}
	});
	mnMen.add(rdbtnmntmNodeid);
	//rdbtnmntmNodeid.setSelected(nodeid);
	mnMen.add(mntmBeenden);
	
	JMenu mnNewMenu = new JMenu("Hilfe");
	menuBar.add(mnNewMenu);
	
	JMenuItem mntmNewMenuItem = new JMenuItem("Hilfe");
	mntmNewMenuItem.addActionListener(new ActionListener() 
	{
		public void actionPerformed(ActionEvent er) {
			/* Hilfeseite öffnen */
			try {
			    open();
			} catch (URISyntaxException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}
	});	
	
	mnNewMenu.add(mntmNewMenuItem);
	
	JMenuItem mntmNewMenuItem_1 = new JMenuItem("\u00DCber");
	mntmNewMenuItem_1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			/* Über Button */
			JOptionPane.showMessageDialog(null, "CanOpen Rechner \nV"
					+ strVersionsNummer + " von Julian Reddemann");
			    }
		
	});
	mnNewMenu.add(mntmNewMenuItem_1);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);

	
	lblNodeID.setBounds(10, 12, 133, 14);
	contentPane.add(lblNodeID);

	
	tfNodeID.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
		    if(tfNodeID.equals("")){
		    	
		    }else {
			pruefeEingabe(1);
		    }
		}
	    }
	});
	tfNodeID.setBounds(10, 37, 123, 20);
	contentPane.add(tfNodeID);
	tfNodeID.setColumns(10);
	tfNodeID.setToolTipText("Hier bitte die Node-ID im Bereich 0-128 eingeben");

	
	btnUmrechnen.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		pruefeEingabe(1);
	    }
	});
	btnUmrechnen.setBounds(143, 36, 113, 23);
	contentPane.add(btnUmrechnen);

	
	lblPdo.setBounds(10, 82, 64, 14);
	contentPane.add(lblPdo);

	
	lblPdo_1.setBounds(10, 107, 64, 14);
	contentPane.add(lblPdo_1);

	
	lblPdo_2.setBounds(10, 132, 64, 14);
	contentPane.add(lblPdo_2);

	
	lblPdo_3.setBounds(10, 157, 64, 14);
	contentPane.add(lblPdo_3);

	
	lblSdo.setBounds(186, 82, 68, 14);
	contentPane.add(lblSdo);

	
	lblSdo_1.setBounds(186, 107, 70, 14);
	contentPane.add(lblSdo_1);

	
	lblSdo_2.setBounds(186, 132, 68, 14);
	contentPane.add(lblSdo_2);

	
	lblSdo_3.setBounds(186, 157, 70, 14);
	contentPane.add(lblSdo_3);

	
	lblEmergency.setBounds(10, 182, 64, 14);
	contentPane.add(lblEmergency);

	
	lblHeartbeat.setBounds(10, 207, 64, 14);
	contentPane.add(lblHeartbeat);

	lblPDO1Ergebnis.setBounds(84, 82, 92, 14);
	contentPane.add(lblPDO1Ergebnis);
	lblPDO1Ergebnis.setToolTipText("PDO1(tx) = 180/hex + Node-ID");

	lblPDO2Ergebnis.setBounds(84, 107, 92, 14);
	contentPane.add(lblPDO2Ergebnis);
	lblPDO2Ergebnis.setToolTipText("PDO2(tx) = 280/hex + Node-ID");

	lblPDO3Ergebnis.setBounds(84, 132, 92, 14);
	contentPane.add(lblPDO3Ergebnis);
	lblPDO3Ergebnis.setToolTipText("PDO3(tx) = 380/hex + Node-ID");

	lblPDO4Ergebnis.setBounds(84, 157, 92, 14);
	contentPane.add(lblPDO4Ergebnis);
	lblPDO4Ergebnis.setToolTipText("PDO4(tx) = 480/hex + Node-ID");

	lblSDO1Ergebnis.setBounds(248, 82, 73, 14);
	contentPane.add(lblSDO1Ergebnis);
	lblSDO1Ergebnis.setToolTipText("PDO1(rx) = 200/hex + Node-ID");

	lblSDO2Ergebnis.setBounds(248, 107, 73, 14);
	contentPane.add(lblSDO2Ergebnis);
	lblSDO2Ergebnis.setToolTipText("PDO2(rx) = 300/hex + Node-ID");

	lblSDO3Ergebnis.setBounds(248, 132, 73, 14);
	contentPane.add(lblSDO3Ergebnis);
	lblSDO3Ergebnis.setToolTipText("PDO3(rx) = 400/hex + Node-ID");

	lblSDO4Ergebnis.setBounds(248, 157, 73, 14);
	contentPane.add(lblSDO4Ergebnis);
	lblSDO4Ergebnis.setToolTipText("PDO4(rx) = 500/hex + Node-ID");

	lblEmergencyErgebnis.setBounds(84, 182, 92, 14);
	contentPane.add(lblEmergencyErgebnis);
	lblEmergencyErgebnis.setToolTipText("Emergency = 80/hex + Node-ID");

	lblHeartbeatErgebnis.setBounds(84, 207, 92, 14);
	contentPane.add(lblHeartbeatErgebnis);
	lblHeartbeatErgebnis.setToolTipText("Heartbeat = 700/hex + Node-ID");



	
	lblSdotx.setBounds(186, 181, 70, 16);
	contentPane.add(lblSdotx);

	
	lblSdorx.setBounds(186, 206, 70, 16);
	contentPane.add(lblSdorx);

	lblSDOtx.setBounds(248, 182, 73, 16);
	contentPane.add(lblSDOtx);
	lblSDOtx.setToolTipText("SDO(tx) = 580/hex + Node-ID");

	lblSDOrx.setBounds(248, 205, 73, 16);
	contentPane.add(lblSDOrx);
	lblSDOrx.setToolTipText("SDO(rx) = 600/hex + Node-ID");

	
	tfHextoNode.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
		    pruefeEingabe(2);
		}
	    }
	});
	tfHextoNode.setBounds(10, 37, 123, 20);
	contentPane.add(tfHextoNode);
	tfHextoNode.setColumns(10);
	tfHextoNode
		.setToolTipText("Hier bitte die Cob-ID in hexadezimaler Schreibweise eintragen");

	
	btnUmrechnen2.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		pruefeEingabe(2);
	    }
	});
	btnUmrechnen2.setBounds(143, 36, 113, 23);
	contentPane.add(btnUmrechnen2);

	
	lblTyp.setBounds(11, 68, 46, 14);
	contentPane.add(lblTyp);

	lblNodeTyp.setBounds(77, 71, 99, 14);
	contentPane.add(lblNodeTyp);

	
	lblNewLabel_1.setBounds(10, 86, 46, 14);
	contentPane.add(lblNewLabel_1);

	lblNodeIDErgebnis.setBounds(77, 86, 99, 14);
	contentPane.add(lblNodeIDErgebnis);

	JDesktopPane desktopPane = new JDesktopPane();
	desktopPane.setBounds(160, 54, 211, -51);
	contentPane.add(desktopPane);

	
	lblNodeidErmitteln.setBounds(10, 12, 123, 14);
	contentPane.add(lblNodeidErmitteln);


    }

 
   



	private void umrechnen() {
	// Berechnen der IDs in Abhängigkeit der Node-ID
	int nodeid = Integer.parseInt(tfNodeID.getText()); // Zwischenspeichern
							   // des Eingabewertes

	// Berechnen PDO1 (180/hex) == 384(dec)
	int intPDO1 = nodeid + 384;
	String PDO1 = Integer.toHexString(intPDO1);
	lblPDO1Ergebnis.setText(PDO1.toUpperCase());

	int intPDO2 = nodeid + 640;
	String PDO2 = Integer.toHexString(intPDO2);
	lblPDO2Ergebnis.setText(PDO2.toUpperCase());

	int intPDO3 = nodeid + 896;
	String PDO3 = Integer.toHexString(intPDO3);
	lblPDO3Ergebnis.setText(PDO3.toUpperCase());

	int intPDO4 = nodeid + 1152;
	String PDO4 = Integer.toHexString(intPDO4);
	lblPDO4Ergebnis.setText(PDO4.toUpperCase());

	int intSDO1 = nodeid + 512;
	String SDO1 = Integer.toHexString(intSDO1);
	lblSDO1Ergebnis.setText(SDO1.toUpperCase());

	int intSDO2 = nodeid + 768;
	String SDO2 = Integer.toHexString(intSDO2);
	lblSDO2Ergebnis.setText(SDO2.toUpperCase());

	int intSDO3 = nodeid + 1024;
	String SDO3 = Integer.toHexString(intSDO3);
	lblSDO3Ergebnis.setText(SDO3.toUpperCase());

	int intSDO4 = nodeid + 1280;
	String SDO4 = Integer.toHexString(intSDO4);
	lblSDO4Ergebnis.setText(SDO4.toUpperCase());

	int intEmergency = nodeid + 128; // hex 80?
	String Emergency = Integer.toHexString(intEmergency);
	lblEmergencyErgebnis.setText(Emergency.toUpperCase());

	int intHeartbeat = nodeid + 1792; // hex 700?
	String Heartbeat = Integer.toHexString(intHeartbeat);
	lblHeartbeatErgebnis.setText(Heartbeat.toUpperCase());

	int intSDOtx = nodeid + 1408; // hex 580
	String SDOtx = Integer.toHexString(intSDOtx);
	lblSDOtx.setText(SDOtx.toUpperCase());

	int intSDOrx = nodeid + 1536; // hex 600
	String SDOrx = Integer.toHexString(intSDOrx);
	lblSDOrx.setText(SDOrx.toUpperCase());

	tfNodeID.requestFocus();
	tfNodeID.selectAll();

    }

    private void pruefeEingabe(int check) {

	switch (check) {
	case 1: {

	    // Prüfen ob Feldinhalt im Bereich 0-127 ist
	    String pruefeFeld = tfNodeID.getText();

	    // Prüfe ob Kommazahl
	    if ((Double.parseDouble(pruefeFeld)
		    - (int) Double.parseDouble(pruefeFeld) == 0)) {
		if (Integer.parseInt(pruefeFeld) <= 127
			&& Integer.parseInt(pruefeFeld) >= 0) {
		    umrechnen();
		} else {
		    JOptionPane.showMessageDialog(null,
			    "Geben Sie die Node-ID im Bereich 0-127 ein");
		    tfNodeID.requestFocus();
		    tfNodeID.selectAll();
		}
	    } else {
		JOptionPane.showMessageDialog(null,
			"Geben Sie eine Ganzzahl ein!");
		tfNodeID.requestFocus();
		tfNodeID.selectAll();
	    }
	    break;
	}
	case 2: {

	    String hexValue = tfHextoNode.getText();
	    int inthexValue = Integer.parseInt(hexValue, 16);
	    int NodeID = 0;

	    if (inthexValue == 0) {
		// NMT
		lblNodeTyp.setText("NMT");
		NodeID = 0;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue == 128) {
		// Synchronisation
		lblNodeTyp.setText("Synchronisation");
		NodeID = 128;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 129 && inthexValue <= 255) {
		// Emergency
		lblNodeTyp.setText("Emergency");
		NodeID = inthexValue - 128;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 385 && inthexValue <= 511) {
		// PDO1 tx
		lblNodeTyp.setText("PDO1 (tx)");
		NodeID = inthexValue - 384;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));

	    } else if (inthexValue >= 513 && inthexValue <= 639) {
		// PDO1 rx
		lblNodeTyp.setText("PDO1 (rx)");
		NodeID = inthexValue - 512;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 641 && inthexValue <= 767) {
		// PDO2 tx
		lblNodeTyp.setText("PDO2 (tx)");
		NodeID = inthexValue - 640;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 769 && inthexValue <= 895) {
		// PDO2 rx
		lblNodeTyp.setText("PDO2 (rx)");
		NodeID = inthexValue - 768;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 897 && inthexValue <= 1023) {
		// PDO3 tx
		lblNodeTyp.setText("PDO3 (tx)");
		NodeID = inthexValue - 896;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1025 && inthexValue <= 1151) {
		// PDO3 rx
		lblNodeTyp.setText("PDO3 (rx)");
		NodeID = inthexValue - 1024;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1153 && inthexValue <= 1279) {
		// PDO4 tx
		lblNodeTyp.setText("PDO4 (tx)");
		NodeID = inthexValue - 1152;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1281 && inthexValue <= 1407) {
		// PDO4 rx
		lblNodeTyp.setText("PDO4 (rx)");
		NodeID = inthexValue - 1280;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1409 && inthexValue <= 1535) {
		// SDO senden
		lblNodeTyp.setText("SDO (tx)");
		NodeID = inthexValue - 1408;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1537 && inthexValue <= 1663) {
		// SDO empfangen
		lblNodeTyp.setText("SDO (rx)");
		NodeID = inthexValue - 1536;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1793 && inthexValue <= 1919) {
		// NMT Error (Heartbeat)
		lblNodeTyp.setText("Heartbeat");
		NodeID = inthexValue - 1792;
		lblNodeIDErgebnis.setText(Integer.toString(NodeID));
	    } else if (inthexValue >= 1920 || inthexValue < 0) {
		// Fehlermeldung nicht innerhalb der Werte
		String Fehler = "Fehler!";
		lblNodeTyp.setText(Fehler);
		lblNodeIDErgebnis.setText(Fehler);

	    } else {
		// Fehlermeldung
		String Fehler = "Fehler!";
		lblNodeTyp.setText(Fehler);
		lblNodeIDErgebnis.setText(Fehler);
	    }
	    tfHextoNode.requestFocus();
	    tfHextoNode.selectAll();
	    break;

	
	}
	}
    }
	public void umschalten(int parameter) {
		boolean xcobid = true;
		boolean xnodeid = false;
		if (parameter==1) {
			/** Cob-ID **/
			xcobid = true;
			xnodeid = false;

			
		}else{
		/** Node-ID**/	
			xcobid = false;
			xnodeid = true;
			

		}
		
		rdbtnmntmCobid.setSelected(xcobid);
		rdbtnmntmNodeid.setSelected(xnodeid);
		btnUmrechnen.setVisible(xnodeid);
		lblEmergency.setVisible(xcobid);
		lblNodeidErmitteln.setVisible(xnodeid);
		lblNodeID.setVisible(xcobid);
		
		
	    lblEmergency.setVisible(xcobid);
	    lblEmergencyErgebnis.setVisible(xcobid);
	    lblHeartbeatErgebnis.setVisible(xcobid);
	    lblPDO1Ergebnis.setVisible(xcobid);
	    lblPDO2Ergebnis.setVisible(xcobid);
	    lblPDO3Ergebnis.setVisible(xcobid);
	    lblPDO4Ergebnis.setVisible(xcobid);
	    lblSDO1Ergebnis.setVisible(xcobid);
	    lblSDO2Ergebnis.setVisible(xcobid);
	    lblSDO3Ergebnis.setVisible(xcobid);
	    lblSDO4Ergebnis.setVisible(xcobid);
	    lblSDOrx.setVisible(xcobid);
	    lblSDOtx.setVisible(xcobid);
	    lblNodeIDErgebnis.setVisible(xnodeid);
	    lblNodeTyp.setVisible(xnodeid);
		lblNodeidErmitteln.setVisible(xnodeid);
		lblNewLabel_1.setVisible(xnodeid);
		lblTyp.setVisible(xnodeid);
		tfHextoNode.setVisible(xnodeid);
		lblSdorx.setVisible(xcobid);
		lblSdotx.setVisible(xcobid);
		lblPdo.setVisible(xcobid);
		lblPdo_1.setVisible(xcobid);
		lblPdo_2.setVisible(xcobid);
		lblPdo_3.setVisible(xcobid);
		lblSdo.setVisible(xcobid);
		lblSdo_1.setVisible(xcobid);
		lblSdo_2.setVisible(xcobid);
		lblHeartbeat.setVisible(xcobid);
		lblNodeID.setVisible(xcobid);
		tfNodeID.setVisible(xcobid);
		btnUmrechnen.setVisible(xcobid);
		btnUmrechnen2.setVisible(xnodeid);
		lblSdo_3.setVisible(xcobid);
		

	}
	
    }
